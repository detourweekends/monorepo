# Install cockroach

```
$ kubectl create -f cockroachdb-statefulset-secure.yaml
# wait 1 min
$ kubectl get csr | grep cockroachdb
# verify pod count (3)
$ kubectl certificate approve default.node.cockroachdb-0 default.node.cockroachdb-1 default.node.cockroachdb-2
$ kubectl create -f cluster-init-secure.yaml
$ kubectl create -f client-secure.yaml
# verify
$ kubectl exec -it cockroachdb-client-secure -- ./cockroach sql --certs-dir=/cockroach-certs --host=cockroachdb-public
$ kubectl port-forward cockroachdb-0 8080
$ open http://localhost:8080
```

