# Create fanout ingress
```
$ kubectl apply -f fanout-ingress.yaml
$ gcloud beta compute backend-services update <NAME_OF_BACKEND_SERVICE> \
--custom-request-header 'x-client-region:{client_region}' \
--custom-request-header 'x-client-region-subdivision:{client_region_subdivision}' \
--custom-request-header 'x-client-city:{client_city}' \
--custom-request-header 'x-client-city-lat-long:{client_city_lat_long}'
```
FROM: https://cloud.google.com/compute/docs/load-balancing/http/backend-service#variables_that_can_appear_in_the_header_value
