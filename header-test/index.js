const express = require('express');
const app = express();
app.set('json spaces', 2);
app.all('*', (req, res) => res.json({ ...req.headers, "ip_": req.ip, "ips_": req.ips }));
const server = app.listen(process.env.PORT || 3000, () => console.log(`listenign on ${server.address().port}`));
