CREATE TABLE "users" (
    "id" UUID DEFAULT gen_random_uuid(),
    "username" STRING NOT NULL,
    PRIMARY KEY ("id"),
    UNIQUE ("username")
);

CREATE TABLE "auth"."tokens" (
    "id" UUID DEFAULT gen_random_uuid(),
    "algo" STRING NOT NULL,
    "hash" STRING NOT NULL,
    "active" BOOL NOT NULL DEFAULT 'false',
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT current_timestamp(),
    "user_id" UUID NOT NULL,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("user_id") REFERENCES "auth"."users"("id") ON DELETE CASCADE
);

