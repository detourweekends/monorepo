CREATE TABLE "live"."events" (
    "id" UUID DEFAULT gen_random_uuid(),
    "type" STRING NOT NULL,
    "payload" JSONB NOT NULL,
    "user_id" UUID,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("user_id") REFERENCES "auth"."users"("id") ON DELETE SET NULL
);


