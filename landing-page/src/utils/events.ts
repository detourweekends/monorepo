import { values } from 'lodash';

export interface Event {
  startDate: Date;
  endDate: Date;
  category: string;
  name: string;
  id: string;
}

export const setDateWithDiff = ({ months = 0, days = 0, date }: { months?: number, days?: number, date?: number}) => {
  const tempDate = new Date();
  tempDate.setHours(0,0,0,0);
  tempDate.setMonth(tempDate.getMonth() + months);
  tempDate.setDate(tempDate.getDay() + days);
  if (date) {
    tempDate.setDate(date);
  }

  return tempDate;
};

export const eventsToday = (date: Date, events: Event[]): Event[] =>
  events.filter(event => ((event.startDate.valueOf() <= date.valueOf())) && (date.valueOf() <= event.endDate.valueOf()));

export const groupByCategory = (events: Event[]): Event[][] =>
  values(events.reduce((acc, event) => {
    if (acc[event.category]) {
      acc[event.category] = acc[event.category].concat(event);
    } else {
      acc[event.category] = [ event ];
    }
    return acc;
  }, {} as { [key: string]: Event[] }));