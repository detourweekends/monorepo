import { uniq } from 'lodash';
import * as React from 'react';
import { RouteProps } from 'react-router';

import Button from '../components/button/button';
import ButtonGroup from '../components/button/button-group';
import Calendar from '../components/calendar/calendar';
import Hero from '../components/hero/hero';
import List from '../components/list/list';
import Navbar from '../components/navbar/navbar';
import { Event, setDateWithDiff } from '../utils/events';

import './Landing.css';

const events: Event[] = [{
  category: 'climbing',
  endDate: setDateWithDiff({ date: 28 }),
  id: '1',
  name: 'Climb weekend in Big Sur',
  startDate: setDateWithDiff({ date: 25 }),
},
{
  category: 'climbing',
  endDate: setDateWithDiff({ date: 19 }),
  id: '2',
  name: 'Saturday at the crag',
  startDate: setDateWithDiff({ date: 18 }),
},
{
  category: 'kayaking',
  endDate: setDateWithDiff({ date: 19 }),
  id: '3',
  name: 'Kayak Big Sur',
  startDate: setDateWithDiff({ date: 18 }),
},
{
  category: 'hiking',
  endDate: setDateWithDiff({ months: 1 }),
  id: '4',
  name: 'Hike the backcountry of Santa Cruz',
  startDate: setDateWithDiff({ months: 1, days: 3 }),
}];

interface State {
  allEvents: Event[];
  selectedEvents: Event[];
  categories: string[];
}

/* tslint:disable jsx-no-lambda */

export default class Landing extends React.Component<RouteProps, State> {
  constructor(props: RouteProps) {
    super(props);
    this.state = {
      allEvents: events,
      categories: uniq(events.map(ev => ev.category)),
      selectedEvents: events,
    };
  }

  public render() {
    return (
      <div className="landing-page">
        <Navbar />
        <Hero />
        <div className="content">
          <ButtonGroup>
            <Button text="Hiking" onClick={() => this.toggleCat('hiking')} />
            <Button text="Climbing" onClick={() => this.toggleCat('climbing')} />
            <Button text="Kayaking" onClick={() => this.toggleCat('kayaking')} />
            <Button text="Whitewater rafting" onClick={() => this.toggleCat('rafting')} />
            <Button text="Canyoneering" onClick={() => this.toggleCat('canyon')} />
          </ButtonGroup>
          <div className="split">
            <Calendar events={this.state.allEvents.filter(ev => this.state.categories.find(cat => cat === ev.category))} />
            <List events={this.state.allEvents.filter(ev => this.state.categories.find(cat => cat === ev.category))} />
          </div>
        </div>
        
      </div>
    );
  }

  private toggleCat = (cat: string) => 
    this.state.categories.length === 1
    ? this.setState({ categories: uniq(this.state.allEvents.map(ev => ev.category)) })
    : this.setState({ categories: [cat] });

}