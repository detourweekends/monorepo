import * as React from 'react';
import { Link } from 'react-router-dom';

import Map from '../components/map/map-static';
import './detail.css';

export default class Detail extends React.Component {
  public render() {
    return (
      <div className="detail">
        <div className="menu-bar">
          <div className="left">
            <Link to="/"> {'<'} Back to San Francisco</Link>
          </div>
          <div className="right">
            <img src="" className="profile" />
          </div>
        </div>
        <div className="content">
          <div className="left-right">
            <div className="left">
              <h1>Rock Climbing Beyond Basics at Castle Rock State Park</h1>
              <b>Beyond basics climbing days are full days and appropriate for climbers of any skill level who can tie in and belay competently and confidently at the start of the day. It doesn’t matter if you climb 5.6 or 5.11, we’ll get you set up on climbs that challenge you and help you develop your skills.</b>
              <p>Beyond basics climbing days are full days and appropriate for climbers of any skill level who can tie in and belay competently and confidently at the start of the day. It doesn’t matter if you climb 5.6 or 5.11, we’ll get you set up on climbs that challenge you and help you develop your skills. We’ll focus on getting as many climbs in as possible during the day. Your Outdoor Adventure Club guide will give you tips and instruction throughout the day to coach you through the climbs.</p>
              <p>Indoor climbers, this is a great opportunity to take your gym skills and apply them to real rock! Castle Rock offers remote and beautiful climbing in the 5.6-5.11 level. The park has many climbing areas including the famed Goat Rock, Castle Rock, Cal Cliffs, the Underworld, and the Waterfalls. Our guides will determine where you climb based on the the overall experience of the group.</p>
              <p>Group size will be limited to a maximum of 6 climbers per guide, so early sign up is advised. The trip lasts from 8:30 am to approximately 5:00 pm, longer than any other guide service. After climbing you can grab some food with your fellow climbers in Saratoga. It’s a perfect way to finish the day, trade stories from your climbs, and develop new friendships.</p>
              <h3>What's Included</h3>
              <ul>
                <li>A day of professionally guided rock climbing</li>
                <li>Professional guides trained by the American Mountain Guides Association</li>
                <li>All gear including shoes, harness, belay devices, helmets</li>
              </ul>
              <h3>Experience needed</h3>
              <ul>
                <li>10 days in the gym</li>
              </ul>
            </div>
            <div className="right">
              <div className="images">
                <img src="/detail_1.jpg" className="detail-image" />
              </div>
              <div className="price">
                $150 / person
              </div>
              <div className="cta">
                <div>Book Trip</div>
              </div>
              <div className="map">
                <Map lat={37.224} lon={-122.09146} zoom={6} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}