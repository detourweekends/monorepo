import * as React from 'react';
import { Route, Switch } from 'react-router';

import './App.css';
import Detail from './pages/detail';
import Landing from './pages/Landing';

class App extends React.Component {
  public render() {
    return (
      <Switch>
        <Route path='/trip/:id'>
          <Detail />
        </Route>
        <Route component={Landing} />
      </Switch>
    );
  }
}

export default App;
