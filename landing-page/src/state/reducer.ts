import { Action } from 'redux';

type Acitvity = 'Climbing'| 'Hiking';

export interface State {
  featuredActivities: Acitvity[];
}

export const initialState: State = {
  featuredActivities: ['Climbing', 'Hiking'],
};

export const reducer = (state = initialState, action: Action) => {
  switch (action.type) {
    default:
      return state;
  }
};
