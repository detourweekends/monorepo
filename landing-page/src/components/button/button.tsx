import * as React from 'react';

import './button.css';

export interface Props {
  text: string;
  onClick: () => any;
}

export default (props: Props) => 
  <div className="button">
    <div className="button-content" onClick={props.onClick}>
      <div className="photo" />
      <div className="text">{props.text}</div>
    </div>
  </div>;
