import * as React from 'react';

const style: React.CSSProperties = {
  display: 'flex',
  overflowX: 'hidden',
}

export default (props: React.Props<{}>) => 
  <div style={style}>
    {props.children}
  </div>;
