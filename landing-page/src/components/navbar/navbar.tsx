import * as React from 'react';

import './navbar.css';

export default class NavBar extends React.Component {
  public render() {
    return (
      <div className="navbar">
        <div className="bar">
          <input type="text" className="nav-search" value="San Francisco, CA" />
        </div>
        <div className="padding" />
      </div>
    );
  }
}