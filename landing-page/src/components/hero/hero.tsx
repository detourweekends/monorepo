import * as React from 'react';

const style: React.CSSProperties = {
  alignItems: 'center',
  background: 'url(climbing.jpg)',
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  display: 'flex',
  height: 300,
  position: 'relative',
  width: '100%',
};

const textStyle: React.CSSProperties = {
  backgroundColor: 'rgba(255,255,255, 0.9)',
  borderRadius: 2,
  height: 100,
  marginLeft: 15,
  padding: 10,
  position: 'absolute',
  width: 200,
}

export default () =>
  <div style={style}>
    <div style={textStyle}>
      <div>Climb the Alps</div>
      <div>Spend 3 nights on the mountain staring at the stars and exhausted from the walks</div>
    </div>
  </div>
;
