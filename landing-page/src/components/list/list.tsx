import * as React from 'react';

import { Event } from '../../utils/events';
import './list.css';
import Row from './row';

interface Props {
  events: Event[];
}

export default class List extends React.Component<Props> {
  public render() {
    return (
      <div className="events-list">
        {this.props.events.map(event => <Row key={event.name} event={event} />)}
      </div>
    );
  }
}