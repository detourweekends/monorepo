import * as React from 'react';
import { Link } from 'react-router-dom';

import { Event } from '../../utils/events';
import './row.css';

interface Props {
  event: Event;
}

export default class Row extends React.Component<Props> {
  public render() {
    return (
      <Link className="button event-row" to={`/trip/${this.props.event.id}`}>
        <div className="button-content">
          <div className="photo" />
          <div className="text">{this.props.event.name}</div>
        </div>
      </Link>
    );
  }
}