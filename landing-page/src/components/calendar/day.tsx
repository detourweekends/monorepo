import * as React from 'react';

import { Event, eventsToday, groupByCategory } from '../../utils/events';
import Activity from './activity';
import './day.css';

interface Props {
  valid: boolean;
  date: Date;
  events: Event[];
}

export default class Day extends React.Component<Props> {
  public render() {
    const eventsTodayGrouped = groupByCategory(eventsToday(this.props.date, this.props.events));
    console.log(this.props.date, eventsToday(this.props.date, this.props.events), eventsTodayGrouped); // tslint:disable-line
    return (
      <div className={`day ${this.props.valid ? 'valid' : ''}`}>
        <div className="date">{this.props.date.getDate()}</div>
        {eventsTodayGrouped.map(events => <Activity key={events[0].category} count={events.length} text={events[0].category} />)}
      </div>
    );
  }
}