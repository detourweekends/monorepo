import { range } from 'lodash';
import * as moment from 'moment';
import * as React from 'react';

import { Event } from '../../utils/events';
import Day from './day';
import './week.css';

interface Props {
  weekNo: number;
  month: number;
  events: Event[];
}

export default class Week extends React.Component<Props> {
  public render() {
    const baseDate = moment().month(this.props.month).week(this.props.weekNo).hour(0).minute(0).second(0).millisecond(0);
    return (
      <div className="week">
        {range(7).map(val => 
          <Day
            key={val}
            valid={baseDate.weekday(val).toDate().valueOf() >= moment().toDate().valueOf()}
            date={baseDate.weekday(val).toDate()}
            events={this.props.events}
          />)}
      </div>
    );
  }
}