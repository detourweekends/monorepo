import * as React from 'react';

import './activity.css';

interface Props {
  count?: number;
  text?: string;
}

export default class Activity extends React.Component<Props> {
  public static defaultProps = {
    count: 1,
    text: 'climbing'
  }

  public render() {
    return (
      <div className="activity">
        <div className="activity-count">{this.props.count}</div>
        <div className="activity-text">{this.props.text}</div>
      </div>
    );
  }
}