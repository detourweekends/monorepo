import { range } from 'lodash';
import * as moment from 'moment';
import * as React from 'react';

import { Event } from '../../utils/events';
import './calendar.css';
import Week from './week';

interface Props {
  events: Event[];
}

export default class Calendar extends React.Component<Props,{}> {
  public render() {
    return (
      <div className="calendar">
        {range(moment().weeks(), moment().weeks() + 5)
          .map(weekNo => <Week key={weekNo} weekNo={weekNo} month={moment().startOf('month').months()} events={this.props.events} />)}
      </div>
    );
  }
}