import * as mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import * as React from 'react';

interface Props {
  lat: number;
  lon: number;
  zoom: number;
}

export default class Map extends React.Component<Props> {
  private mapContainer: Element | undefined = undefined;
  public componentDidMount() {
    (mapboxgl as any).accessToken = 'pk.eyJ1IjoiYWRhbWp1aGFzeiIsImEiOiJjamhpODFod3Mwd3cwM2NrMzhxeHc4dXZuIn0.pn1V4DChbvB22eckx6qO3g';
    const map = new mapboxgl.Map({
      center: [this.props.lon, this.props.lat],
      container: this.mapContainer,
      interactive: false,
      style: 'mapbox://styles/adamjuhasz/cjhi8dj1q09df2rrq6q68vhyv',
      zoom: this.props.zoom,
    });
    new mapboxgl.Marker({
      color: '#394553'
    } as any)
      .setLngLat([this.props.lon, this.props.lat])
      .addTo(map);
  }

  public render() {
    return (
      <div ref={el => this.mapContainer = el as Element}   />
    );
  }
}