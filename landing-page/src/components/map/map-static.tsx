import * as React from 'react';

import './map-static.css';

interface Props {
  lat: number;
  lon: number;
  zoom: number;
}

export default class Map extends React.Component<Props> {
  public render() {
    const url = `https://api.mapbox.com/styles/v1/adamjuhasz/cjhi8dj1q09df2rrq6q68vhyv/static/pin-s+394553(${this.props.lon},${this.props.lat})/${this.props.lon},${this.props.lat},${this.props.zoom},0,0/300x200@2x?access_token=pk.eyJ1IjoiYWRhbWp1aGFzeiIsImEiOiJjamhpODFod3Mwd3cwM2NrMzhxeHc4dXZuIn0.pn1V4DChbvB22eckx6qO3g&logo=false&attribution=false`;
    return (
      <div className="static-map" style={{ backgroundImage: `url("${url}")` }} />
    );
  }
}